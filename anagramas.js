"use strict";

String.prototype.removeAccents = function () {
  return this.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
}

var anagramAnalyzer = function (original, anagram) {
  var foundCharacterClosure = function (character) {
    return character
  };
  return {
    withFoundCharacter: function (closure) {
      foundCharacterClosure = closure;
      return this;
    },
    analyze: function (callback) {
      var notMatchedCharacters = anagram.split("").filter(function (character) {
        return character !== " ";
      });

      var analysis = original
        .split("")
        .map(function (originalChar) {
          var index = notMatchedCharacters.findIndex(function (anagramChar) {
            return originalChar.removeAccents().toLowerCase() === anagramChar.removeAccents().toLowerCase();
          });
          if (index !== -1) {
            notMatchedCharacters.splice(index, 1);
            return foundCharacterClosure(originalChar);
          } else {
            return originalChar;
          }
        })
        .join("");

      callback(analysis, notMatchedCharacters.length > 0 ? notMatchedCharacters : undefined)
      return this;
    },
  }
}

var originalInput = document.getElementById("original");
var anagramInput = document.getElementById("anagram");

var analyze = function () {
  anagramAnalyzer(originalInput.value, anagramInput.value)
    .withFoundCharacter(function (character) {
      return "<span class='found'>" + character + "</span>";
    })
    .analyze(function (analysis, notMatchedCharacters) {
      var content = analysis;
      if (notMatchedCharacters) {
        content += "<p class='error'>Characters not found: '" + notMatchedCharacters + "'</p>";
      }
      document.getElementById("analysis").innerHTML = content;
    });
};

originalInput.addEventListener("input", analyze);
anagramInput.addEventListener("input", analyze);
analyze();